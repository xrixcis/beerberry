#include "catch.hpp"
#include "pumpcontroller.h"

#include <sstream>
#include <thread>

TEST_CASE("ControllerMode can be read from stream") {

  ControllerMode mode;

  SECTION("can read SYS") {
    std::istringstream is("SYS");
    REQUIRE(is >> mode);
    REQUIRE(mode == ControllerMode::SYS);
  }

  SECTION("can read WIRINGPI") {
    std::istringstream is("WIRINGPI");
    REQUIRE(is >> mode);
    REQUIRE(mode == ControllerMode::WIRINGPI);
  }

  SECTION("can read DUMMY") {
    std::istringstream is("DUMMY xxx");
    REQUIRE(is >> mode);
    REQUIRE(mode == ControllerMode::DUMMY);
  }

  SECTION("fails on bad input") {
    std::istringstream is("xx");
    REQUIRE_FALSE(is >> mode);
  }
}

TEST_CASE("PumpController tracks pump state") {

  Configuration::Pump conf;
  conf.mode(ControllerMode::DUMMY);
  conf.pin(0);
  conf.maxRunTime(1);
  conf.coolDownTime(1);

  PumpController controller{conf};

  REQUIRE(controller.getStatus() == PumpController::Status::STOPPED);
  controller.start();
  std::this_thread::sleep_for(std::chrono::milliseconds{100});

  SECTION("starts the pump") {
    REQUIRE(controller.getStatus() == PumpController::Status::RUNNING);
  }

  SECTION("stops the pump") {
    controller.stop();
    std::this_thread::sleep_for(std::chrono::milliseconds{100});
    REQUIRE(controller.getStatus() == PumpController::Status::STOPPED);
  }

  SECTION("cools down the pump") {
    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
    REQUIRE(controller.getStatus() == PumpController::Status::RUNNING_WAIT);
  }

  SECTION("stops the pump during cooldown") {
    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
    REQUIRE(controller.getStatus() == PumpController::Status::RUNNING_WAIT);
    controller.stop();
    std::this_thread::sleep_for(std::chrono::milliseconds{100});
    REQUIRE(controller.getStatus() == PumpController::Status::STOPPED);
  }

  SECTION("restarts the pump") {
    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
    REQUIRE(controller.getStatus() == PumpController::Status::RUNNING_WAIT);
    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
    REQUIRE(controller.getStatus() == PumpController::Status::RUNNING);
  }
}

TEST_CASE("PumpController works with PWM") {

  Configuration::Pump conf;
  conf.mode(ControllerMode::DUMMY);
  conf.maxRunTime(1);
  conf.coolDownTime(1);

  SECTION("with PWM on") {
    conf.pin(PumpController::PWM_PIN);
    conf.pwmCycle(10);
    PumpController controller{conf};

    REQUIRE(controller.isPwm());

    controller.start();
    std::this_thread::sleep_for(std::chrono::milliseconds{100});

    SECTION("writes the pwm value") {
      REQUIRE(controller.getDummyModeValue() == 10);
    }

    SECTION("writes 0 when stopped") {
      controller.stop();
      std::this_thread::sleep_for(std::chrono::milliseconds{100});
      REQUIRE(controller.getDummyModeValue() == PumpController::PWM_MIN);
    }
  }

  SECTION("Does not turn on PWM on bad pins") {
    conf.pin(PumpController::PWM_PIN + 1);
    conf.pwmCycle(10);
    PumpController controller{conf};

    REQUIRE_FALSE(controller.isPwm());
  }

  SECTION("Does not turn on PWM if duty cycle is max") {
    conf.pin(PumpController::PWM_PIN);
    conf.pwmCycle(PumpController::PWM_MAX);
    PumpController controller{conf};

    REQUIRE_FALSE(controller.isPwm());
  }

  SECTION("Clamps max value") {
    conf.pin(PumpController::PWM_PIN);
    conf.pwmCycle(PumpController::PWM_MAX + 1);
    PumpController controller{conf};

    REQUIRE_FALSE(controller.isPwm());
  }
}
