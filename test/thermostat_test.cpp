#include "catch.hpp"
#include "thermostat.h"

TEST_CASE("Thermostat when") {
  const float TEMPERATURE_THRESHOLD = 1.0F;
  const float COOLANT_DIFF_START = 5.0F;
  const float COOLANT_DIFF_STOP = 2.0F;

  Configuration::Thermostat conf;
  conf.threshold(TEMPERATURE_THRESHOLD);
  conf.coolantDiffStartCooling(COOLANT_DIFF_START);
  conf.coolantDiffStopCooling(COOLANT_DIFF_STOP);
  Thermostat t{conf};

  Thermostat::Status newStatus = Thermostat::Status::OFF;
  t.setOnStateChangedListener(
      [&newStatus](auto status) { newStatus = status; });

  SECTION("is unitialized") {
    SECTION("requires target temperature to be set") {
      t.setCurrentTemperature(20.0F);
      t.setCoolantTemperature(10.0F);
      REQUIRE(newStatus == Thermostat::Status::OFF);
      t.setTargetTemperature(20.0F);
      REQUIRE_FALSE(newStatus == Thermostat::Status::OFF);
    }

    SECTION("requires current temperature to be set") {
      t.setTargetTemperature(20.0F);
      t.setCoolantTemperature(10.0F);
      REQUIRE(newStatus == Thermostat::Status::OFF);
      t.setCurrentTemperature(20.0F);
      REQUIRE_FALSE(newStatus == Thermostat::Status::OFF);
    }

    SECTION("requires coolant temperature to be set") {
      t.setTargetTemperature(20.0F);
      t.setCurrentTemperature(20.0F);
      REQUIRE(newStatus == Thermostat::Status::OFF);
      t.setCoolantTemperature(20.0F);
      REQUIRE_FALSE(newStatus == Thermostat::Status::OFF);
    }
  }

  SECTION("is initialized") {
    const float TARGET_TEMP = 20.0F;
    const float CURRENT_TEMP = TARGET_TEMP + TEMPERATURE_THRESHOLD / 2.0F;

    t.setTargetTemperature(TARGET_TEMP);
    t.setCurrentTemperature(CURRENT_TEMP);
    t.setCoolantTemperature(0.0F);

    SECTION("is in standby when the temperature is ok") {
      REQUIRE(newStatus == Thermostat::Status::STANDBY);
    }

    SECTION("and temperature is over threshold") {
      auto temperatureOver = TARGET_TEMP + TEMPERATURE_THRESHOLD + 10.0F;
      t.setCurrentTemperature(temperatureOver);

      SECTION("is cooling when the temperature is over thershold") {
        REQUIRE(newStatus == Thermostat::Status::COOLING);
      }

      SECTION("keeps cooling until the temperature drops below the target") {
        auto temperatureUnder = TARGET_TEMP - TEMPERATURE_THRESHOLD - 0.1F;

        t.setCurrentTemperature(TARGET_TEMP);
        REQUIRE(newStatus == Thermostat::Status::COOLING);
        t.setCurrentTemperature(temperatureUnder);
        REQUIRE(newStatus == Thermostat::Status::STANDBY);
      }

      SECTION("stops cooling when coolant temperature is too high") {
        t.setCoolantTemperature(temperatureOver - COOLANT_DIFF_STOP + 0.1F);
        REQUIRE(newStatus == Thermostat::Status::DISABLED);
      }

      SECTION("resumes cooling when coolant temperature drops") {
        auto coolantOverStart = temperatureOver - COOLANT_DIFF_START + 0.1F;
        auto coolantUnderStart = temperatureOver - COOLANT_DIFF_START - 0.1F;

        t.setCoolantTemperature(temperatureOver);
        REQUIRE(newStatus == Thermostat::Status::DISABLED);
        t.setCoolantTemperature(coolantOverStart);
        REQUIRE(newStatus == Thermostat::Status::DISABLED);
        t.setCoolantTemperature(coolantUnderStart);
        REQUIRE(newStatus == Thermostat::Status::COOLING);
      }
    }

    SECTION("goes to disabled when coolant temperature is too high") {
      auto coolantTemp = CURRENT_TEMP - COOLANT_DIFF_STOP + 0.1F;

      REQUIRE(newStatus == Thermostat::Status::STANDBY);
      t.setCoolantTemperature(coolantTemp);
      REQUIRE(newStatus == Thermostat::Status::DISABLED);
    }

    SECTION("goes back to standby when coolant temperature drops") {
      auto coolantTempOver = CURRENT_TEMP - COOLANT_DIFF_START + 0.1F;
      auto coolantTempUnder = CURRENT_TEMP - COOLANT_DIFF_START - 0.1F;

      REQUIRE(newStatus == Thermostat::Status::STANDBY);
      t.setCoolantTemperature(CURRENT_TEMP);
      REQUIRE(newStatus == Thermostat::Status::DISABLED);
      t.setCoolantTemperature(coolantTempOver);
      REQUIRE(newStatus == Thermostat::Status::DISABLED);
      t.setCoolantTemperature(coolantTempUnder);
      REQUIRE(newStatus == Thermostat::Status::STANDBY);
    }
  }
}
