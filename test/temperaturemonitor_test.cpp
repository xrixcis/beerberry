#include "catch.hpp"
#include "temperaturemonitor.h"

#include <fstream>

namespace fs = std::filesystem;

static auto busDir = fs::temp_directory_path() / "test_bus";

void createSensor(const std::string &name,
                  const std::string &data =
                      "ca 01 4b 46 7f FF 06 10 65 : crc=65 YES\n"
                      "ca 01 4b 46 7f FF 06 10 65 t=26456\n") {
  fs::create_directories(busDir / name);
  std::ofstream of(busDir / name / "w1_slave");
  of << data;
}

static

    auto
    conf(std::map<std::string, std::string> &aliases) {
  Configuration::Sensors c;
  c.aliases(aliases);
  c.directory(busDir);
  return c;
}

TEST_CASE("TemperatureMonitor initializes sensors") {

  std::map<std::string, std::string> aliases;
  fs::remove_all(busDir);
  fs::create_directories(busDir);

  SECTION("ignores the master device") {
    createSensor("w1_bus_master1");
    TemperatureMonitor monitor{conf(aliases)};
    REQUIRE(monitor.sensorsCount() == 0);
  }

  SECTION("can alias sensor name") {
    createSensor("28-000007c2951e");
    aliases["28-000007c2951e"] = "alias";
    TemperatureMonitor monitor{conf(aliases)};
    REQUIRE(monitor.getTemperature("alias"));
  }
}

TEST_CASE("TemperatureMonitor reads sensors") {

  fs::remove_all(busDir);
  fs::create_directories(busDir);

  std::map<std::string, std::string> aliases;

  SECTION("can update sensor temperature") {
    createSensor("28-000007c2951e");
    TemperatureMonitor monitor{conf(aliases)};
    REQUIRE(monitor.updateTemperatures());
    auto temp = monitor.getTemperature("28-000007c2951e");
    REQUIRE(temp);
    REQUIRE(*temp == Approx(26.456));
  }

  SECTION("can report sensor fail") {
    createSensor("28-000007c2951e");
    createSensor("28-000007c2951f", "");
    TemperatureMonitor monitor{conf(aliases)};
    REQUIRE_FALSE(monitor.updateTemperatures());
  }

  SECTION("does invoke global callback") {
    createSensor("28-000007c2951e");
    TemperatureMonitor monitor{conf(aliases)};
    bool invoked = false;
    monitor.subscribeChanges("28-000007c2951e",
                             [&invoked](const std::string &name, float val,
                                        Thermometer::Status status) {
                               REQUIRE(name == "28-000007c2951e");
                               REQUIRE(val == Approx(26.456F));
                               REQUIRE(status == Thermometer::Status::OK);
                               invoked = true;
                             });
    monitor.updateTemperatures();
    REQUIRE(invoked);
  }

  SECTION("does invoke sensor callback") {
    createSensor("28-000007c2951e");
    TemperatureMonitor monitor{conf(aliases)};
    bool invoked = false;
    monitor.subscribeChanges("28-000007c2951e",
                             [&invoked](const std::string name, float val,
                                        Thermometer::Status status) {
                               REQUIRE(name == "28-000007c2951e");
                               REQUIRE(val == Approx(26.456F));
                               REQUIRE(status == Thermometer::Status::OK);
                               invoked = true;
                             });
    monitor.updateTemperatures();
    REQUIRE(invoked);
  }
}
