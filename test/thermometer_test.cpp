#include "catch.hpp"
#include "temperaturemonitor.h"
#include <fstream>

namespace fs = std::filesystem;

static auto sensorDir = fs::temp_directory_path() / "test_sensor";
static auto sensorFile = sensorDir / "w1_slave";
void thermometerData(const std::string &data) {
  std::ofstream of(sensorFile);
  of << data;
}

TEST_CASE("Thermometer reads sensor values") {
  fs::create_directories(sensorDir);
  fs::remove(sensorFile);

  Thermometer t{sensorDir, "alias"};

  SECTION("can read the sensor temperature") {
    thermometerData("ca 01 4b 46 7f FF 06 10 65 : crc=65 YES\n"
                    "ca 01 4b 46 7f FF 06 10 65 t=26456\n");
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::OK);
    REQUIRE(t2.getValue() == Approx(26.456));
  }

  SECTION("sets the fail flag if sensor reading has unexpected format") {
    thermometerData("ca 01 4b 46 7f FF 06 10 65 : crc=65 YES\n"
                    "ca 01 4b 46 7f FF 06 10 65 t=26xx456\n");
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::INPUT_ERROR);
  }

  SECTION("sets the fail flag if sensor reports read failed") {
    thermometerData("ca 01 4b 46 7f FF 06 10 65 : crc=65 NO\n"
                    "ca 01 4b 46 7f FF 06 10 65 t=26456\n");
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::SENSOR_FAIL);
  }

  SECTION("sets the fail flag if device file is missing") {
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::OPEN_FAIL);
  }

  SECTION("sets the fail flag if file read failed") {
    thermometerData("ca 01 4b 46 7f FF 06 10 65 : crc=65 YES");
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::READ_FAIL);
  }

  SECTION("can read temperature around zero") {
    thermometerData("ca 01 4b 46 7f FF 06 10 65 : crc=65 YES\n"
                    "ca 01 4b 46 7f FF 06 10 65 t=500\n");
    Thermometer t2 = t.updateTemperature();
    REQUIRE(t2.getStatus() == Thermometer::Status::OK);
    REQUIRE(t2.getValue() == Approx(.5));
  }
}
