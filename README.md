# beerberry


## Systemd service

The service not only starts the executable, but also clones/updates the repo, 
rebuilds the executable if necesssary and installs everything to /tmp/beerberry.

Service file is located in the service directory, copy it to /etc/systemd/system directory.

The service needs a helper script beerberry_build, copy it to /usr/bin with 
execute permission. The script also needs configuration file passphrase stored in /etc/beerberry.passprase

