#include "beerberry.h"
#include "thermostat.h"
#include <boost/log/trivial.hpp>
#include <chrono>
#include <ctime>
#include <thread>

int BeerBerry::run() {

  setUpThermostatControl();
  setUpTemperatureLogging();

  return loop();
}

void BeerBerry::updateTemperatures() { tempMonitor.updateTemperatures(); }

void BeerBerry::setUpThermostatControl() {

  if (!configuration.thermostat().enabled()) {
    BOOST_LOG_TRIVIAL(info) << "Thermostat is disabled";
    return;
  }

  mqtt.subscribeTopic(
      configuration.messaging().tempControlTopic(), [this](auto, auto msg) {
        BOOST_LOG_TRIVIAL(debug)
            << "Received thermostat control messaage: " << msg;
        try {
          thermostat.setTargetTemperature(std::stof(msg));
        } catch (std::logic_error &e) {
          BOOST_LOG_TRIVIAL(error) << "Thermostat control message " << msg
                                   << " could not be parsed: " << e.what();
        }
      });

  tempMonitor.subscribeChanges(
      configuration.thermostat().currentSensor(),
      [this](const std::string &, float temp, Thermometer::Status status) {
        if (status == Thermometer::Status::OK) {
          thermostat.setCurrentTemperature(temp);
        }
      });

  tempMonitor.subscribeChanges(
      configuration.thermostat().coolantSensor(),
      [this](const std::string &, float temp, Thermometer::Status status) {
        if (status == Thermometer::Status::OK) {
          thermostat.setCoolantTemperature(temp);
        }
      });

  thermostat.setOnStateChangedListener([this](auto status) {
    switch (status) {
    case Thermostat::Status::STANDBY:
      [[fallthrough]];
    case Thermostat::Status::DISABLED:
      pump.stop();
      break;
    case Thermostat::Status::COOLING:
      pump.start();
      break;
    default:
      break;
    }
    BOOST_LOG_TRIVIAL(debug) << "Logging thermostat status change: "
                             << Thermostat::statusToStr(status);
    mqtt.sendMessage(configuration.messaging().stateChangeTopic(),
                     withTimestamp(Thermostat::statusToStr(status)));
  });
}

void BeerBerry::setUpTemperatureLogging() {

  tempMonitor.subscribeChanges([this](auto name, auto temp, auto status) {
    BOOST_LOG_TRIVIAL(debug)
        << "Read sensor " << name << ", temperature is " << temp
        << ", status is " << Thermometer::statusToStr(status);
    if (status == Thermometer::Status::OK && log) {
      mqtt.sendMessage(configuration.messaging().loggingTopic() + "/" + name,
                       withTimestamp(std::to_string(temp)));
    }
  });
}

int BeerBerry::loop() {

  std::chrono::seconds updateFreq{configuration.sensors().updateFrequency()};
  std::chrono::seconds logFreq{configuration.sensors().logFrequency()};

  while (true) {
    updateTime = std::chrono::steady_clock::now();
    log = (updateTime - lastLogTime) > logFreq;
    updateTemperatures();
    if (log) {
      lastLogTime = updateTime;
      mqtt.sendMessage(
          configuration.messaging().loggingTopic() + "/target",
          withTimestamp(std::to_string(thermostat.getTargetTemperature())));
    }
    std::this_thread::sleep_until(updateTime + updateFreq);
  }
}

std::string BeerBerry::withTimestamp(const std::string &msg) {
  std::time_t t = std::time(nullptr);
  char buff[100];

  if (std::strftime(buff, sizeof(buff), "%FT%T%z", std::localtime(&t))) {
    return std::string{buff} + " " + msg;
  }
  return msg;
}
