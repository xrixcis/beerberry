#include "runner.h"
#include "beerberry.h"
#include "pumpcontroller.h"
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/syslog_backend.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/exception_handler.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <iostream>
#include <mosquittopp.h>

namespace po = boost::program_options;
namespace log = boost::log;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
Runner::Runner() { mosqpp::lib_init(); }

Runner::~Runner() { mosqpp::lib_cleanup(); }
#pragma GCC diagnostic pop

int Runner::run(int argc, char *const argv[]) {

  if (!parseArgs(argc, argv)) {
    return 1;
  }

  if (!initLogging()) {
    return 2;
  }

  configuration.sensors().aliases(getSensorAliases());

  BeerBerry b{configuration};

  return b.run();
}

po::options_description commandLineOptions() {
  po::options_description opts("Generic options");
  opts.add_options()("help,h", "produce help message")(
      "config,c", po::value<std::string>(), "configuration file path");
  return opts;
}

po::options_description Runner::loggingOptions() {
  po::options_description opts;
  opts.add_options()("log.syslog_target",
                     po::value<std::string>()->default_value("127.0.0.1"),
                     "Syslog target address")(
      "log.syslog_port", po::value<unsigned short>()->default_value(514),
      "Syslog port")("log.syslog_level",
                     po::value<std::string>()->default_value(DEFAULT_LOG_LEVEL),
                     "Syslog log level, 'off' turns of syslog logging")(
      "log.file_level",
      po::value<std::string>()->default_value(DEFAULT_LOG_LEVEL),
      "Log file log level, 'off' turns of file logging")(
      "log.file_name",
      po::value<std::string>()->default_value(DEFAULT_LOG_FILE),
      "Log file name")(
      "log.file_dir",
      po::value<std::string>()->default_value(DEFAULT_LOG_DIRECTORY),
      "Log file directory")(
      "log.files_keep", po::value<int>()->default_value(DEFAULT_LOG_FILES_KEEP),
      "How many log files to keep")(
      "log.console_level",
      po::value<std::string>()->default_value(DEFAULT_LOG_LEVEL),
      "Console log level, 'off' turns of console logging");
  return opts;
}

po::options_description Runner::sensorOptions() {
  po::options_description opts;
  opts.add_options()(
      "sensors.bus_path",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.sensors().directory(val); }),
      "Temp sensors bus directory path")(
      "sensors.update_frequency",
      po::value<uint32_t>()->default_value(60)->notifier(
          [this](auto val) { configuration.sensors().updateFrequency(val); }),
      "Sensor update frequency in seconds")(
      "sensors.log_frequency",
      po::value<uint32_t>()->default_value(15 * 60)->notifier(
          [this](auto val) { configuration.sensors().logFrequency(val); }),
      "Sensor logging frequency in seconds")(
      "sensors.alias.*", po::value<std::string>(), "Sensor alias");
  return opts;
}

po::options_description Runner::brokerOptions() {
  po::options_description opts;
  opts.add_options()(
      "broker.host",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.messaging().host(val); }),
      "MQTT broker host")(
      "broker.port",
      po::value<int>()->default_value(1883)->notifier(
          [this](auto val) { configuration.messaging().port(val); }),
      "MQTT broker port")(
      "broker.username",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.messaging().username(val); }),
      "MQTT broker username")(
      "broker.password",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.messaging().password(val); }),
      "MQTT broker password")(
      "broker.logging_topic",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.messaging().loggingTopic(val); }),
      "MQTT status message topic")(
      "broker.thermostat_temp_topic",
      po::value<std::string>()->required()->notifier([this](auto val) {
        configuration.messaging().tempControlTopic(val);
      }),
      "MQTT topic for thermostat temperature control")(
      "broker.thermostat_state_topic",
      po::value<std::string>()->required()->notifier([this](auto val) {
        configuration.messaging().stateChangeTopic(val);
      }),
      "MQTT topic for thermostat state changes")(
      "broker.qos",
      po::value<int>()->default_value(0)->notifier(
          [this](auto val) { configuration.messaging().qos(val); }),
      "MQTT persistent session")(
      "broker.persistent_session",
      po::value<bool>()->default_value(true)->notifier([this](auto val) {
        configuration.messaging().persistentSession(val);
      }),
      "MQTT persistent session")(
      "broker.client_id", po::value<std::string>()->notifier([this](auto val) {
        configuration.messaging().clientId(val);
      }),
      "MQTT client id");
  return opts;
}

po::options_description Runner::pumpOptions() {
  po::options_description opts;
  opts.add_options()("pump.mode",
                     po::value<ControllerMode>()->required()->notifier(
                         [this](auto val) { configuration.pump().mode(val); }),
                     "Pump control mode - WIRINGPI (requires root) or"
                     " SYS (requires the pin to be exported)")(
      "pump.pin", po::value<int>()->required()->notifier([this](auto val) {
        configuration.pump().pin(val);
      }),
      "WiringPi pin to which the pump is connected")(
      "pump.max_run_time",
      po::value<int>()->required()->notifier(
          [this](auto val) { configuration.pump().maxRunTime(val); }),
      "Maximum pump run time before cool down in seconds")(
      "pump.cool_down_time",
      po::value<int>()->required()->notifier(
          [this](auto val) { configuration.pump().coolDownTime(val); }),
      "Pump cool down time in seconds")(
      "pump.pwm_duty_cycle",
      po::value<int>()->required()->default_value(1024)->notifier(
          [this](auto val) { configuration.pump().pwmCycle(val); }),
      "Pump speed PWM control, 0-1024");
  return opts;
}

po::options_description Runner::thermostatOptions() {
  po::options_description opts;
  opts.add_options()("thermostat.enabled",
                     po::value<bool>()->required()->notifier([this](auto val) {
                       configuration.thermostat().enabled(val);
                     }),
                     "Enable/disable thermostat")(
      "thermostat.current_temp_sensor",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.thermostat().currentSensor(val); }),
      "Current temperature sensor")(
      "thermostat.coolant_temp_sensor",
      po::value<std::string>()->required()->notifier(
          [this](auto val) { configuration.thermostat().coolantSensor(val); }),
      "Coolant temperature sensor")(
      "thermostat.threshold",
      po::value<float>()->required()->notifier(
          [this](auto val) { configuration.thermostat().threshold(val); }),
      "Thermostat switching threshold")(
      "thermostat.coolant_diff.start_cooling",
      po::value<float>()->required()->notifier([this](auto val) {
        configuration.thermostat().coolantDiffStartCooling(val);
      }),
      "If coolant temperature is below target temperature by at least this "
      "value, the thermostat can start cooling")(
      "thermostat.coolant_diff.stop_cooling",
      po::value<float>()->required()->notifier([this](auto val) {
        configuration.thermostat().coolantDiffStopCooling(val);
      }),
      "If the temperature is below target by less than this value, the "
      "thermostat gets disabled");
  return opts;
}

bool Runner::parseArgs(int argc, char *const argv[]) {

  po::options_description configOpts("Configuration");
  configOpts.add(loggingOptions())
      .add(sensorOptions())
      .add(brokerOptions())
      .add(pumpOptions())
      .add(thermostatOptions());

  po::options_description opts("Allowed options");
  opts.add(commandLineOptions()).add(configOpts);

  try {
    po::store(po::parse_command_line(argc, argv, opts), options);
    if (options.count("config")) {
      std::string config = options["config"].as<std::string>();
      po::store(po::parse_config_file<char>(config.c_str(), configOpts),
                options);
    }

    if (options.count("help")) {
      std::cerr << opts << "\n";
      return false;
    }

    po::notify(options);

  } catch (po::error &e) {
    std::cerr << e.what() << "\n\n";
    std::cerr << opts << "\n";
    return false;
  }

  return true;
}

std::map<std::string, std::string> Runner::getSensorAliases() {
  std::string aliasPrefix = "sensors.alias.";
  std::map<std::string, std::string> aliases;
  std::for_each(options.lower_bound(aliasPrefix), options.end(),
                [&](std::pair<std::string, po::variable_value> a) {
                  if (a.first.find(aliasPrefix) == 0) {
                    aliases[a.first.substr(aliasPrefix.length())] =
                        a.second.as<std::string>();
                  }
                });
  return aliases;
}

static std::optional<log::trivial::severity_level>
getLevel(const std::string &sev) {
  if (sev == "off") {
    return {};
  }
  log::trivial::severity_level severity{};
  std::istringstream is(sev);
  if (!(is >> severity)) {
    throw std::logic_error("Unparseable log severity: " + sev);
  }
  return severity;
}

static void initConsoleLog(log::trivial::severity_level level,
                           const std::string &format) {
  auto backend = log::add_console_log(std::clog, log::keywords::format = format,
                                      log::keywords::filter =
                                          log::trivial::severity >= level);
}

static void initFileLog(log::trivial::severity_level level,
                        const std::string &file, const std::string &dir,
                        const int keepFiles, const std::string &format) {
  auto backend = log::add_file_log(
      log::keywords::file_name = file, log::keywords::target = dir,
      log::keywords::auto_flush = true,
      log::keywords::rotation_size = 10 * 1024 * 1024,
      log::keywords::time_based_rotation =
          log::sinks::file::rotation_at_time_point(0, 0, 0),
      log::keywords::max_files = keepFiles, log::keywords::format = format,
      log::keywords::filter = (log::trivial::severity >= level));
}

static void initSyslogLog(log::trivial::severity_level level,
                          const std::string &target, unsigned short port) {
  boost::shared_ptr<log::core> core = log::core::get();

  // Create a backend
  boost::shared_ptr<log::sinks::syslog_backend> backend(
      new log::sinks::syslog_backend(
          log::keywords::facility = log::sinks::syslog::facility::user,
          log::keywords::use_impl = log::sinks::syslog::udp_socket_based));

  backend->set_target_address(target, port);

  log::sinks::syslog::custom_severity_mapping<log::trivial::severity_level>
      mapping("Severity");
  mapping[log::trivial::trace] = log::sinks::syslog::debug;
  mapping[log::trivial::debug] = log::sinks::syslog::debug;
  mapping[log::trivial::info] = log::sinks::syslog::info;
  mapping[log::trivial::warning] = log::sinks::syslog::warning;
  mapping[log::trivial::error] = log::sinks::syslog::error;
  mapping[log::trivial::fatal] = log::sinks::syslog::critical;
  backend->set_severity_mapper(mapping);

  auto sink = boost::make_shared<
      log::sinks::synchronous_sink<log::sinks::syslog_backend>>(backend);
  sink->set_filter(log::trivial::severity >= level);
  // Suppress syslog exceptions, which can occur e.g. in case the target network
  // is unreachable. Otherwise the exception is not caught and leads to
  // terminate()
  sink->set_exception_handler(log::make_exception_suppressor());
  core->add_sink(sink);
}

bool Runner::initLogging() {

  log::register_simple_formatter_factory<log::trivial::severity_level, char>(
      "Severity");

  auto format = "[%TimeStamp%] %ThreadID% <%Severity%>: %Message%";

  log::add_common_attributes();

  auto fileLevel = getLevel(options["log.file_level"].as<std::string>());

  if (fileLevel.has_value()) {
    initFileLog(*fileLevel, options["log.file_name"].as<std::string>(),
                options["log.file_dir"].as<std::string>(),
                options["log.files_keep"].as<int>(), format);
  }

  auto consoleLevel = getLevel(options["log.console_level"].as<std::string>());

  if (consoleLevel.has_value()) {
    initConsoleLog(*consoleLevel, format);
  }

  auto syslogLevel = getLevel(options["log.syslog_level"].as<std::string>());

  if (syslogLevel.has_value()) {
    initSyslogLog(*syslogLevel, options["log.syslog_target"].as<std::string>(),
                  options["log.syslog_port"].as<unsigned short>());
  }

  BOOST_LOG_TRIVIAL(info) << "Logging initialized";
  return true;
}
