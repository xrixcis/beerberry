#include "mosquittoclient.h"
#include <boost/log/trivial.hpp>
#include <stdexcept>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

MosquittoClient::MosquittoClient(MosquittoClient::Observer &messageObserver,
                                 const char *clientId, bool cleanSession)
    : mosquittopp(clientId, cleanSession), observer(messageObserver) {
  reconnect_delay_set(1, MAX_RECONNECT_DELAY, true);
  loop_start();
}

MosquittoClient::~MosquittoClient() {
  disconnect();
  loop_stop();
}

void MosquittoClient::connect(const std::string &host, int port) {
  auto err = connect_async(host.c_str(), port);
  if (err) {
    throw std::logic_error(std::string("MQTT connection failed: ") +
                           mosqpp::strerror(err));
  }
}

void MosquittoClient::on_connect(int rc) {
  if (!rc) {
    BOOST_LOG_TRIVIAL(debug) << "MQTT connection established";
  } else {
    BOOST_LOG_TRIVIAL(debug)
        << "MQTT connection failed: " << mosqpp::strerror(rc);
  }
}

void MosquittoClient::on_message(const mosquitto_message *message) {
  std::string payload(static_cast<char *>(message->payload),
                      static_cast<size_t>(message->payloadlen));
  observer.onMessage(message->topic, payload);
}

void MosquittoClient::on_disconnect(int rc) {
  if (!rc) {
    BOOST_LOG_TRIVIAL(debug) << "MQTT disconnect successful";
  } else {
    BOOST_LOG_TRIVIAL(debug)
        << "MQTT unexpected disconnect: " << mosqpp::strerror(rc);
  }
}

void MosquittoClient::on_log(int level, const char *str) {
  switch (level) {
  case MOSQ_LOG_INFO:
    BOOST_LOG_TRIVIAL(info) << "Mosquitto: " << str;
    break;
  case MOSQ_LOG_NOTICE:
    BOOST_LOG_TRIVIAL(info) << "Mosquitto: " << str;
    break;
  case MOSQ_LOG_WARNING:
    BOOST_LOG_TRIVIAL(warning) << "Mosquitto: " << str;
    break;
  case MOSQ_LOG_ERR:
    BOOST_LOG_TRIVIAL(error) << "Mosquitto: " << str;
    break;
  default:
    BOOST_LOG_TRIVIAL(debug) << "Mosquitto: " << str;
    break;
  }
}
