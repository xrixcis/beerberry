#include "thermostat.h"
#include <boost/log/trivial.hpp>
#include <cmath>

Thermostat::Thermostat(const Configuration::Thermostat &conf) {
  threshold = conf.threshold();
  coolantDiffStop = conf.coolantDiffStopCooling();
  coolantDiffStart = conf.coolantDiffStartCooling();
}

void Thermostat::setTargetTemperature(float temp) {
  std::lock_guard lock{mutex};
  this->targetTemperature = temp;
  update();
}

float Thermostat::getTargetTemperature() {
  std::lock_guard lock{mutex};
  return targetTemperature;
}

void Thermostat::setCurrentTemperature(float temp) {
  std::lock_guard lock{mutex};
  this->currentTemperature = temp;
  update();
}

void Thermostat::setCoolantTemperature(float temp) {
  std::lock_guard lock{mutex};
  this->coolantTemperature = temp;
  update();
}

void Thermostat::setOnStateChangedListener(
    const std::function<void(Thermostat::Status)> &listener) {
  stateChangeListener = listener;
}

std::string Thermostat::statusToStr(Thermostat::Status status) {
  switch (status) {
  case Status::OFF:
    return "OFF";
  case Status::STANDBY:
    return "STANDBY";
  case Status::COOLING:
    return "COOLING";
  case Status::DISABLED:
    return "DISABLED";
  }
}

void Thermostat::update() {
  // don't start until everything is initialized
  if (isOff()) {
    if (!areAllTemperaturesSet()) {
      return;
    } else {
      setState(Status::STANDBY);
    }
  }

  if (isCoolantOverThreshold()) {
    setState(Status::DISABLED);
  } else if (isTemperatureOverThreshold()) {
    setState(Status::COOLING);
  } else {
    setState(Status::STANDBY);
  }

  BOOST_LOG_TRIVIAL(debug) << "Thermostat current " << currentTemperature
                           << ", target " << targetTemperature << ", coolant "
                           << coolantTemperature << ", status "
                           << statusToStr(status);
}

void Thermostat::setState(Thermostat::Status status) {
  Status old = this->status;
  this->status = status;
  if (stateChangeListener && old != status) {
    (*stateChangeListener)(status);
  }
}

bool Thermostat::isCooling() { return status == Status::COOLING; }

bool Thermostat::isOff() { return status == Status::OFF; }

bool Thermostat::isCoolantOverThreshold() {
  return (isCooling() && isCoolantOverStopThreshold()) ||
         (!isCooling() && isCoolantOverStartThreshold());
}

bool Thermostat::isCoolantOverStopThreshold() {
  return coolantTemperature >= (currentTemperature - coolantDiffStop);
}

bool Thermostat::isCoolantOverStartThreshold() {
  return coolantTemperature >= (currentTemperature - coolantDiffStart);
}

bool Thermostat::isTemperatureOverThreshold() {
  // cool until we reach the low threshold
  return (isCooling() && isTemperatureOverLowThreshold()) ||
         isTemperatureOverHighThreshold();
}

bool Thermostat::isTemperatureOverLowThreshold() {
  return currentTemperature >= (targetTemperature - threshold);
}

bool Thermostat::isTemperatureOverHighThreshold() {
  return currentTemperature >= (targetTemperature + threshold);
}

bool Thermostat::areAllTemperaturesSet() {
  return !(std::isnan(currentTemperature) || std::isnan(targetTemperature) ||
           std::isnan(coolantTemperature));
}
