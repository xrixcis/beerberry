#include "include/pumpcontroller.h"

#include <algorithm>
#include <atomic>
#include <boost/log/trivial.hpp>
#include <csignal>

#include "config.h"

#ifdef HAVE_WIRINGPI
#include <wiringPi.h>
#else
#include "dummywiringpi.h"
#endif

static std::atomic_int PUMP_PIN{-1};

extern "C" void signal_handler(int sig) {
  auto pin = PUMP_PIN.load();
  if (pin >= 0) {
    digitalWrite(pin, 0);
    if (pin == PumpController::PWM_PIN) {
      pwmWrite(pin, PumpController::PWM_MIN);
    }
  }
  signal(sig, SIG_DFL);
  raise(sig);
}

PumpController::PumpController(const Configuration::Pump &conf) {
  mode = conf.mode();
  maxRunTime = std::chrono::seconds{conf.maxRunTime()};
  coolDownTime = std::chrono::seconds{conf.coolDownTime()};
  pwmDutyCycle = std::clamp(conf.pwmCycle(), PWM_MIN, PWM_MAX);
  switch (mode) {
  case ControllerMode::WIRINGPI:
    setUpWiringpi(conf);
    break;
  case ControllerMode::SYS:
    setUpSys(conf);
    break;
  case ControllerMode::DUMMY:
    setUpDummy(conf);
    break;
  }
  watcher = std::thread{[this]() { watch(); }};

  // Register signal handler & pin number to shut down the pump on abnormal
  // program termination
  PUMP_PIN.store(pin);
  for (auto sig : {SIGABRT, SIGSEGV, SIGFPE, SIGILL, SIGINT, SIGTERM}) {
    std::signal(sig, signal_handler);
  }
}

PumpController::~PumpController() {
  {
    std::lock_guard lock{mutex};
    status = Status::DESTROY;
  }

  condition.notify_all();

  if (watcher.joinable()) {
    watcher.join();
  }
}

void PumpController::start() {
  {
    std::lock_guard<std::mutex> lock{mutex};
    if (status == Status::STOPPED) {
      BOOST_LOG_TRIVIAL(debug) << "Requesting pump start";
      nextStatus = Status::RUNNING;
    }
  }
  condition.notify_all();
}

void PumpController::stop() {
  {
    std::lock_guard<std::mutex> lock{mutex};
    if (status == Status::RUNNING || status == Status::RUNNING_WAIT) {
      BOOST_LOG_TRIVIAL(debug) << "Requesting pump stop";
      nextStatus = Status::STOPPED;
    }
  }
  condition.notify_all();
}

PumpController::Status PumpController::getStatus() {
  std::lock_guard<std::mutex> lock{mutex};
  return status;
}

bool PumpController::isPwm() { return pwm; }

int PumpController::getDummyModeValue() { return dummyModeValue; }

void PumpController::setUpWiringpi(const Configuration::Pump &conf) {
  pin = conf.pin();
  pwm = (pin == PWM_PIN && pwmDutyCycle != PWM_MAX);
  BOOST_LOG_TRIVIAL(debug) << "Setting up WiringPi in wiringPi mode on pin "
                           << pin << ", PWM " << pwm << ", duty cycle "
                           << pwmDutyCycle;
  wiringPiSetup();
  if (pwm) {
    pinMode(pin, PWM_OUTPUT);
    write = [this](int what) { pwmWrite(pin, getPwmValue(what)); };
  } else {
    pinMode(pin, OUTPUT);
    write = [this](int what) { digitalWrite(pin, what); };
  }
}

void PumpController::setUpSys(const Configuration::Pump &conf) {
  pin = wpiPinToGpio(conf.pin());
  BOOST_LOG_TRIVIAL(debug) << "Setting up WiringPi in sys mode on pin "
                           << conf.pin() << "(GPIO " << pin << ")";
  wiringPiSetupSys();
  write = [this](int what) { digitalWrite(pin, what); };
}

void PumpController::setUpDummy(const Configuration::Pump &conf) {
  pin = conf.pin();
  pwm = (pin == PWM_PIN && pwmDutyCycle != PWM_MAX);
  BOOST_LOG_TRIVIAL(debug) << "Setting up WiringPi in DUMMY mode, PWM " << pwm
                           << ", duty cycle " << pwmDutyCycle;
  write = [this](int what) {
    dummyModeValue = pwm ? getPwmValue(what) : what;
    BOOST_LOG_TRIVIAL(debug) << "Running in dummy mode, writing "
                             << dummyModeValue << " to pin " << pin;
  };
}

int PumpController::getPwmValue(int val) {
  return val == LOW ? PWM_MIN : pwmDutyCycle;
}

void PumpController::watch() {
  std::unique_lock lock{mutex};
  while (status != Status::DESTROY) {
    std::chrono::seconds waitDuration = std::chrono::hours{24};

    if (shouldStart()) {
      changeTime = std::chrono::steady_clock::now();
      waitDuration = maxRunTime;
      status = Status::RUNNING;
      write(HIGH);
      BOOST_LOG_TRIVIAL(info) << "Starting the pump";
    } else if (shouldStop()) {
      if (nextStatus != Status::STOPPED) {
        changeTime = std::chrono::steady_clock::now();
        waitDuration = coolDownTime;
        status = Status::RUNNING_WAIT;
        write(LOW);
        BOOST_LOG_TRIVIAL(info) << "Stopping the pump to cool down for "
                                << waitDuration.count() << " seconds";
      } else {
        if (status == Status::RUNNING) {
          write(LOW);
          BOOST_LOG_TRIVIAL(info) << "Stopping the pump";
        }
        status = Status::STOPPED;
      }
    }
    condition.wait_for(lock, waitDuration);
  }
  write(LOW);
  BOOST_LOG_TRIVIAL(info) << "Stopping the pump";
}

bool PumpController::shouldStart() {
  return (status == Status::STOPPED && nextStatus == Status::RUNNING) ||
         (status == Status::RUNNING_WAIT &&
          (changeTime + coolDownTime) < std::chrono::steady_clock::now());
}

bool PumpController::shouldStop() {
  return (status != Status::STOPPED && nextStatus == Status::STOPPED) ||
         (status == Status::RUNNING &&
          (changeTime + maxRunTime) < std::chrono::steady_clock::now());
}

std::istream &operator>>(std::istream &is, ControllerMode &mode) {
  std::string str;
  is >> str;
  if (str == "WIRINGPI") {
    mode = ControllerMode::WIRINGPI;
  } else if (str == "SYS") {
    mode = ControllerMode::SYS;
  } else if (str == "DUMMY") {
    mode = ControllerMode::DUMMY;
  } else {
    is.setstate(std::ios_base::failbit);
  }
  return is;
}
