#include "mqttconnector.h"
#include <boost/log/trivial.hpp>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

MQTTConnector::MQTTConnector(const Configuration::Messaging &conf)
    : client(*this, conf.clientId().empty() ? nullptr : conf.clientId().c_str(),
             !conf.persistentSession()) {
  qos = conf.qos();
  client.username_pw_set(conf.username().c_str(), conf.password().c_str());
  client.connect(conf.host(), conf.port());
}

void MQTTConnector::subscribeTopic(
    const std::string &topic,
    std::function<void(const std::string &, const std::string &)> callback) {
  auto res = subscriptions.insert_or_assign(topic, callback);
  if (!res.second) {
    BOOST_LOG_TRIVIAL(warning)
        << "Overwriting subscribed callback for topic " << topic;
  }
  client.subscribe(nullptr, topic.c_str(), qos);
}

void MQTTConnector::sendMessage(const std::string &topic,
                                const std::string &message) {
  auto err =
      client.publish(nullptr, topic.c_str(), static_cast<int>(message.length()),
                     message.c_str(), qos);
  if (err) {
    BOOST_LOG_TRIVIAL(warning) << "Failed to send message to " << topic << ": "
                               << mosqpp::strerror(err);
  }
}

void MQTTConnector::onMessage(const std::string &topic,
                              const std::string &message) {

  auto callback = std::find_if(
      subscriptions.begin(), subscriptions.end(), [&topic](auto p) {
        bool match = false;
        mosqpp::topic_matches_sub(p.first.c_str(), topic.c_str(), &match);
        return match;
      });

  if (callback != subscriptions.end()) {
    callback->second(topic, message);
  } else {
    BOOST_LOG_TRIVIAL(warning)
        << "No callbacks subscribed for message on topic " << topic;
  }
}
