#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <map>
#include <string>

enum class ControllerMode;

class Configuration {

public:
  class Messaging {
    std::string _host;
    std::string _username;
    std::string _password;
    int _port;
    int _qos;
    bool _persistentSession;
    std::string _clientId;
    std::string _loggingTopic;
    std::string _tempControlTopic;
    std::string _stateChangeTopic;

  public:
    void host(const std::string &val) { _host = val; }

    const std::string &host() const { return _host; }

    void username(const std::string &val) { _username = val; }

    const std::string &username() const { return _username; }

    void password(const std::string &val) { _password = val; }

    const std::string &password() const { return _password; }

    void port(int val) { _port = val; }

    int port() const { return _port; }

    void loggingTopic(const std::string &val) { _loggingTopic = val; }

    const std::string &loggingTopic() const { return _loggingTopic; }

    void tempControlTopic(const std::string &val) { _tempControlTopic = val; }

    const std::string &tempControlTopic() const { return _tempControlTopic; }

    void stateChangeTopic(const std::string &val) { _stateChangeTopic = val; }

    const std::string &stateChangeTopic() const { return _stateChangeTopic; }

    void qos(int val) { _qos = val; }

    int qos() const { return _qos; }

    void persistentSession(bool val) { _persistentSession = val; }

    bool persistentSession() const { return _persistentSession; }

    void clientId(const std::string &val) { _clientId = val; }

    const std::string &clientId() const { return _clientId; }
  };

  class Sensors {
    std::string _directory;
    uint32_t _updateFrequency;
    uint32_t _logFrequency;
    std::map<std::string, std::string> _aliases;

  public:
    void directory(const std::string &val) { _directory = val; }

    const std::string &directory() const { return _directory; }

    void updateFrequency(uint32_t val) { _updateFrequency = val; }

    uint32_t updateFrequency() const { return _updateFrequency; }

    void logFrequency(uint32_t val) { _logFrequency = val; }

    uint32_t logFrequency() const { return _logFrequency; }

    void aliases(const std::map<std::string, std::string> &val) {
      _aliases = val;
    }

    const auto &aliases() const { return _aliases; }
  };

  class Pump {
    ControllerMode _mode;
    int _pin;
    int _maxRunTime;
    int _coolDownTime;
    int _pwmCycle;

  public:
    void mode(const ControllerMode &val) { _mode = val; }

    ControllerMode mode() const { return _mode; }

    void pin(const int &val) { _pin = val; }

    int pin() const { return _pin; }

    void maxRunTime(const int &val) { _maxRunTime = val; }

    int maxRunTime() const { return _maxRunTime; }

    void coolDownTime(const int &val) { _coolDownTime = val; }

    int coolDownTime() const { return _coolDownTime; }

    void pwmCycle(const int &val) { _pwmCycle = val; }

    int pwmCycle() const { return _pwmCycle; }
  };

  class Thermostat {
    std::string _currentSensor;
    std::string _coolantSensor;
    float _threshold;
    float _coolantDiffStartCooling;
    float _coolantDiffStopCooling;
    bool _enabled;

  public:
    void currentSensor(const std::string &val) { _currentSensor = val; }

    const std::string &currentSensor() const { return _currentSensor; }

    void coolantSensor(const std::string &val) { _coolantSensor = val; }

    const std::string &coolantSensor() const { return _coolantSensor; }

    void threshold(const float &val) { _threshold = val; }

    float threshold() const { return _threshold; }

    void coolantDiffStartCooling(const float &val) {
      _coolantDiffStartCooling = val;
    }

    float coolantDiffStartCooling() const { return _coolantDiffStartCooling; }

    void coolantDiffStopCooling(const float &val) {
      _coolantDiffStopCooling = val;
    }

    float coolantDiffStopCooling() const { return _coolantDiffStopCooling; }

    void enabled(const bool &val) { _enabled = val; }

    bool enabled() const { return _enabled; }
  };

  Messaging &messaging() { return _messaging; }

  const Messaging &messaging() const { return _messaging; }

  Sensors &sensors() { return _sensors; }

  const Sensors &sensors() const { return _sensors; }

  Pump &pump() { return _pump; }

  const Pump &pump() const { return _pump; }

  Thermostat &thermostat() { return _thermostat; }

  const Thermostat &thermostat() const { return _thermostat; }

private:
  Messaging _messaging;
  Sensors _sensors;
  Pump _pump;
  Thermostat _thermostat;
};

#endif // CONFIGURATION_H
