#ifndef RUNNER_H
#define RUNNER_H

#include "configuration.h"
#include <boost/program_options.hpp>
#include <optional>
#include <string>

class Runner {
  const std::string DEFAULT_LOG_LEVEL = "INFO";
  const std::string DEFAULT_LOG_FILE = "beerberry.%N.log";
  const std::string DEFAULT_LOG_DIRECTORY = ".";
  const int DEFAULT_LOG_FILES_KEEP = 7;

public:
  Runner();
  ~Runner();
  int run(int argc, char *const argv[]);

private:
  boost::program_options::variables_map options;

  Configuration configuration;

  bool parseArgs(int argc, char *const argv[]);

  std::map<std::string, std::string> getSensorAliases();

  bool initLogging();

  boost::program_options::options_description loggingOptions();
  boost::program_options::options_description sensorOptions();
  boost::program_options::options_description brokerOptions();
  boost::program_options::options_description pumpOptions();
  boost::program_options::options_description thermostatOptions();
};

#endif // RUNNER_H
