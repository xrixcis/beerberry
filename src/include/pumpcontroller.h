#ifndef PUMPCONTROLLER_H
#define PUMPCONTROLLER_H

#include "configuration.h"
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

enum class ControllerMode { WIRINGPI, SYS, DUMMY };

class PumpController {

public:
  static constexpr int PWM_PIN = 1;

  static constexpr int PWM_MIN = 0;

  static constexpr int PWM_MAX = 1024;

  enum class Status { STOPPED, RUNNING, RUNNING_WAIT, DESTROY };

  PumpController(const Configuration::Pump &conf);

  ~PumpController();

  void start();

  void stop();

  Status getStatus();

  bool isPwm();

  int getDummyModeValue();

private:
  void setUpWiringpi(const Configuration::Pump &conf);

  void setUpSys(const Configuration::Pump &conf);

  void setUpDummy(const Configuration::Pump &conf);

  int getPwmValue(int val);

  void watch();

  std::function<void(int)> write;

  bool shouldStart();

  bool shouldStop();

  ControllerMode mode;

  int pin;

  bool pwm = false;

  int pwmDutyCycle;

  Status status = Status::STOPPED;

  Status nextStatus = Status::STOPPED;

  std::chrono::steady_clock::time_point changeTime;

  std::mutex mutex;

  std::condition_variable condition;

  std::thread watcher;

  std::chrono::seconds maxRunTime;

  std::chrono::seconds coolDownTime;

  int dummyModeValue;
};

std::istream &operator>>(std::istream &is, ControllerMode &mode);

#endif // PUMPCONTROLLER_H
