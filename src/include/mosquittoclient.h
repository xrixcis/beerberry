#ifndef MOSQUITTOCLIENT_H
#define MOSQUITTOCLIENT_H

#include <mosquittopp.h>

#include <string>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
class MosquittoClient : public mosqpp::mosquittopp {
#pragma GCC diagnostic pop

  const unsigned int MAX_RECONNECT_DELAY = 60;

public:
  class Observer {
  public:
    virtual ~Observer() = default;
    virtual void onMessage(const std::string &topic,
                           const std::string &message) = 0;
  };

  MosquittoClient(MosquittoClient::Observer &messageObserver,
                  const char *clientId, bool cleanSession);
  ~MosquittoClient() override;

  void connect(const std::string &host, int port);

  void on_connect(int rc) override;
  void on_message(const struct mosquitto_message *message) override;
  void on_disconnect(int rc) override;
  void on_log(int level, const char *str) override;

private:
  Observer &observer;
};

#endif // MOSQUITTOCLIENT_H
