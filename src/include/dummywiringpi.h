#ifndef DUMMYWIRINGPI_H
#define DUMMYWIRINGPI_H

#define LOW 0
#define HIGH 1

#define INPUT 0
#define OUTPUT 1
#define PWM_OUTPUT 2

#include <boost/log/trivial.hpp>

int wiringPiSetup(void) {
  BOOST_LOG_TRIVIAL(warning) << "wiringpi stub: wiringPiSetup() called";
  return 0;
}
int wiringPiSetupSys(void) {
  BOOST_LOG_TRIVIAL(warning) << "wiringpi stub: wiringPiSetupSys() called";
  return 0;
}
void digitalWrite(int pin, int val) {
  BOOST_LOG_TRIVIAL(warning)
      << "wiringpi stub: digitalWrite(" << pin << ", " << val << ") called";
}
void pwmWrite(int pin, int value) {
  BOOST_LOG_TRIVIAL(warning)
      << "wiringpi stub: pwmWrite(" << pin << ", " << value << ") called";
}
void pinMode(int pin, int mode) {
  BOOST_LOG_TRIVIAL(warning)
      << "wiringpi stub: pinMode(" << pin << ", " << mode << ") called";
}
int wpiPinToGpio(int) { return 0; }

#endif // DUMMYWIRINGPI_H
