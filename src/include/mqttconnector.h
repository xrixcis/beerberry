#ifndef MQTTCONNECTOR_H
#define MQTTCONNECTOR_H

#include "configuration.h"
#include "mosquittoclient.h"
#include <functional>
#include <map>
#include <mosquittopp.h>
#include <string>

class MQTTConnector : public MosquittoClient::Observer {

  MosquittoClient client;

  std::map<std::string,
           std::function<void(const std::string &, const std::string &)>>
      subscriptions;

  int qos;

public:
  MQTTConnector(const Configuration::Messaging &conf);
  virtual ~MQTTConnector() override = default;

  void subscribeTopic(
      const std::string &topic,
      std::function<void(const std::string &, const std::string &)> callback);

  void sendMessage(const std::string &topic, const std::string &message);

  void onMessage(const std::string &topic, const std::string &message) override;
};

#endif // MQTTCONNECTOR_H
