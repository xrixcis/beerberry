#ifndef THERMOSTAT_H
#define THERMOSTAT_H

#include "configuration.h"
#include <chrono>
#include <functional>
#include <limits>
#include <mutex>
#include <optional>

class Thermostat {

public:
  enum class Status { OFF, STANDBY, COOLING, DISABLED };

  Thermostat(const Configuration::Thermostat &conf);

  ~Thermostat() = default;

  void setTargetTemperature(float temp);

  float getTargetTemperature();

  void setCurrentTemperature(float temp);

  void setCoolantTemperature(float temp);

  void setOnStateChangedListener(
      const std::function<void(Thermostat::Status)> &listener);

  static std::string statusToStr(Status status);

private:
  float threshold;

  float coolantDiffStop;

  float coolantDiffStart;

  std::mutex mutex;

  Status status = Status::OFF;

  float targetTemperature = std::numeric_limits<float>::quiet_NaN();

  float currentTemperature = std::numeric_limits<float>::quiet_NaN();

  float coolantTemperature = std::numeric_limits<float>::quiet_NaN();

  std::optional<std::function<void(Thermostat::Status)>> stateChangeListener;

  void update();

  void setState(Status status);

  bool isCooling();

  bool isOff();

  bool areAllTemperaturesSet();

  bool isCoolantOverStopThreshold();

  bool isCoolantOverStartThreshold();

  bool isCoolantOverThreshold();

  bool isTemperatureOverThreshold();

  bool isTemperatureOverLowThreshold();

  bool isTemperatureOverHighThreshold();
};

#endif // THERMOSTAT_H
