#ifndef THERMOMETER_H
#define THERMOMETER_H

#include <filesystem>
#include <regex>

class Thermometer {

  static const std::regex READ_OK_REGEX;
  static const std::regex TEMPERATURE_REGEX;
  static const std::string DEVICE_FILE;

public:
  enum class Status {
    OK,
    /* sensor file couldn't be opened */
    OPEN_FAIL,
    /* couldn't read sensor output */
    READ_FAIL,
    /* sensor didn't return YES */
    SENSOR_FAIL,
    /* couldn't parse sensor output */
    INPUT_ERROR,
    /* not yet known */
    UNKNOWN
  };

  static std::string statusToStr(Status status);

  Thermometer(const std::filesystem::path &path, const std::string &alias);

  std::string getName() const { return path.filename(); }

  std::string getAlias() const { return alias; }

  std::filesystem::path getPath() const { return path; }

  float getValue() const { return value; }

  Status getStatus() const { return status; }

  Thermometer updateTemperature() const;

private:
  Thermometer(const std::filesystem::path &path, const std::string &alias,
              float value, Status status);

  Thermometer updated(Status status, float value = 0.0F) const;

  std::filesystem::path path;
  std::string alias;
  float value = 0;
  Status status = Status::UNKNOWN;
};

#endif // THERMOMETER_H
