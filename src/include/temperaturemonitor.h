#ifndef TEMPERATUREMONITOR_H
#define TEMPERATUREMONITOR_H

#include "configuration.h"
#include "thermometer.h"
#include <filesystem>
#include <map>
#include <optional>
#include <shared_mutex>
#include <string>
#include <vector>

class TemperatureMonitor {
  using Callback = std::function<void(const std::string &name, float value,
                                      Thermometer::Status status)>;

  const std::string MASTER_DEVICE = "w1_bus_master1";

  std::shared_mutex mutex;

  std::multimap<std::string, Callback> callbacks;

  std::vector<Callback> globalCallbacks;

  std::map<std::string, Thermometer> sensors;

public:
  TemperatureMonitor(const Configuration::Sensors &conf);

  bool updateTemperatures();

  size_t sensorsCount() { return sensors.size(); }

  std::optional<float> getTemperature(const std::string &sensor);

  void subscribeChanges(const Callback &callback);

  void subscribeChanges(const std::string &sensor, const Callback &callback);

private:
  void invokeCallbacks();
};

#endif // TEMPERATUREMONITOR_H
