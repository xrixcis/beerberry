#ifndef BEERBERRY_H
#define BEERBERRY_H

#include "configuration.h"
#include "mqttconnector.h"
#include "pumpcontroller.h"
#include "temperaturemonitor.h"
#include "thermostat.h"
#include <atomic>
#include <string>

class BeerBerry {
public:
  BeerBerry(const Configuration &config)
      : configuration(config), tempMonitor(config.sensors()),
        mqtt(config.messaging()), pump(config.pump()),
        thermostat(config.thermostat()) {}

  int run();

private:
  void updateTemperatures();

  void setUpThermostatControl();

  void setUpTemperatureLogging();

  int loop();

  static std::string withTimestamp(const std::string &msg);

  const Configuration configuration;

  TemperatureMonitor tempMonitor;

  MQTTConnector mqtt;

  PumpController pump;

  Thermostat thermostat;

  std::chrono::steady_clock::time_point updateTime;

  std::chrono::steady_clock::time_point lastLogTime;

  std::atomic_bool log = false;
};

#endif // BEERBERRY_H
