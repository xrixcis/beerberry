#include "runner.h"

int main(int argc, char * const argv[])
{
    Runner r;
    return r.run(argc, argv);
}
