#include "temperaturemonitor.h"
#include <boost/log/trivial.hpp>
#include <filesystem>
#include <stdexcept>
#include <string>

namespace fs = std::filesystem;

TemperatureMonitor::TemperatureMonitor(const Configuration::Sensors &conf) {
  fs::path path(conf.directory());

  if (!fs::exists(path)) {
    throw std::logic_error("Sensor path does not exist: " + path.string());
  }

  if (!fs::is_directory(path)) {
    throw std::logic_error("Sensor path is not a directory: " + path.string());
  }

  for (auto &p : fs::directory_iterator(path)) {
    auto name = p.path().filename();
    if (p.is_directory() && name != MASTER_DEVICE) {
      auto alias = name;
      auto aliasIt = conf.aliases().find(name);
      if (aliasIt != conf.aliases().end()) {
        alias = aliasIt->second;
      }
      BOOST_LOG_TRIVIAL(debug)
          << "Found sensor " << name << " with alias " << alias;
      sensors.emplace(std::make_pair(alias, Thermometer{p.path(), alias}));
    }
  }
}

bool TemperatureMonitor::updateTemperatures() {
  std::vector<Thermometer> readings;

  {
    std::shared_lock lock{mutex};
    for (auto &sensor : sensors) {
      readings.push_back(sensor.second.updateTemperature());
    }
  }

  bool error = false;

  { // update the internal sensor data (write)
    std::unique_lock lock{mutex};
    for (auto &reading : readings) {
      auto thisError = reading.getStatus() != Thermometer::Status::OK;
      error = error || thisError;
      if (thisError) {
        BOOST_LOG_TRIVIAL(error)
            << "Failed to read sensor " << reading.getAlias()
            << ", reason: " << Thermometer::statusToStr(reading.getStatus());
      }
      sensors.insert_or_assign(reading.getAlias(), reading);
    }
  }

  invokeCallbacks();

  return !error;
}

std::optional<float>
TemperatureMonitor::getTemperature(const std::string &sensor) {
  std::shared_lock lock{mutex};
  auto it = sensors.find(sensor);
  if (it == sensors.end()) {
    return {};
  }
  return {it->second.getValue()};
}

void TemperatureMonitor::subscribeChanges(const Callback &callback) {
  globalCallbacks.push_back(callback);
}

void TemperatureMonitor::subscribeChanges(const std::string &sensor,
                                          const Callback &callback) {
  callbacks.insert({sensor, callback});
}

void TemperatureMonitor::invokeCallbacks() {
  // call the registered callbacks (read)
  std::shared_lock lock{mutex};
  for (auto &sPair : sensors) {
    auto &sensor = sPair.second;
    auto caller = [&sensor](const Callback &cb) {
      cb(sensor.getAlias(), sensor.getValue(), sensor.getStatus());
    };
    std::for_each(globalCallbacks.begin(), globalCallbacks.end(), caller);
    auto cbIts = callbacks.equal_range(sPair.first);
    std::for_each(cbIts.first, cbIts.second,
                  [caller](auto p) { caller(p.second); });
  }
}
