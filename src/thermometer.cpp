#include "thermometer.h"
#include <boost/log/trivial.hpp>
#include <fstream>

namespace fs = std::filesystem;

const std::string Thermometer::DEVICE_FILE{"w1_slave"};
const std::regex Thermometer::READ_OK_REGEX{".*YES"};
const std::regex Thermometer::TEMPERATURE_REGEX{"t=([0-9]*)([0-9]{3})$"};

std::string Thermometer::statusToStr(Thermometer::Status status) {
  switch (status) {
  case Status::OK:
    return "OK";
  case Status::OPEN_FAIL:
    return "OPEN_FAIL";
  case Status::READ_FAIL:
    return "READ_FAIL";
  case Status::SENSOR_FAIL:
    return "SENSOR_FAIL";
  case Status::INPUT_ERROR:
    return "INPUT_ERROR";
  case Status::UNKNOWN:
    return "UNKNOWN";
  }
}

Thermometer::Thermometer(const fs::path &path, const std::string &alias) {
  this->path = path;
  this->alias = alias;
}

Thermometer::Thermometer(const fs::path &path, const std::string &alias,
                         float value, Thermometer::Status status) {
  this->path = path;
  this->alias = alias;
  this->value = value;
  this->status = status;
}

Thermometer Thermometer::updateTemperature() const {

  std::ifstream ifs(path / DEVICE_FILE, std::ios::binary);
  if (!ifs.is_open()) {
    return updated(Status::OPEN_FAIL);
  }
  std::string line;
  if (!std::getline(ifs, line)) {
    return updated(Status::READ_FAIL);
  }

  BOOST_LOG_TRIVIAL(trace) << "Sensor " << alias << " status line: " << line;

  if (!std::regex_match(line, READ_OK_REGEX)) {
    return updated(Status::SENSOR_FAIL);
  }

  if (!std::getline(ifs, line)) {
    return updated(Status::READ_FAIL);
  }

  BOOST_LOG_TRIVIAL(trace) << "Sensor " << alias
                           << " temperature line: " << line;

  std::smatch match;

  if (!std::regex_search(line, match, TEMPERATURE_REGEX) || match.size() != 3) {
    return updated(Status::INPUT_ERROR);
  }

  float val = std::stof(match[1].str() + "." + match[2].str());
  return updated(Status::OK, val);
}

Thermometer Thermometer::updated(Thermometer::Status status,
                                 float value) const {
  return {path, alias, value, status};
}
